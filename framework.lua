require('helpers/mouseHelper')
require('helpers/colorHelper')
require('helpers/xmlHelper')
require('helpers/auxHelpers')

function buildStyle(object, style)
    local splittedStyle = style:split(";")

    for _,v in pairs(splittedStyle) do

        local splitKeyValue = v:split(":")
        local key = splitKeyValue[1]

        if key == "fill" then
            object.r, object.g, object.b = hexToRgb(splitKeyValue[2])
        end

        if key == "fill-opacity" or key == "opacity"  then
            object.alpha = splitKeyValue[2] * 255
        end
    end
end


--[[                                        ]]--
--[[        RECURSIVE OBJECT BUILDERS       ]]--
--[[                                        ]]--
--[[                                        ]]--

function buildPathObject(path)
    local pathObject = {}
    local unparsedData = path.xarg.d
    print(unparsedData)
end

function buildImageObject(image)
    local imageObject = {}
    imageObject.xPercent = image.xarg.x / level.width
    imageObject.yPercent = image.xarg.y / level.height
    imageObject.width = image.xarg.width
    imageObject.height = image.xarg.height
    imageObject.r = 255
    imageObject.g = 255
    imageObject.b = 255
    imageObject.alpha = 255

    local href = image.xarg["href"]
    segments = string.split(href,"/")
    imageObject.drawable = love.graphics.newImage(segments[#segments]);

    function imageObject:draw(args)
        args = args or {}
        args.orientation = args.orientation or 0
        args.originx = args.originx or 0
        args.originy = args.originy or 0
        args.scalex = args.scalex or 1
        args.scaley = args.scaley or 1
        args.drawOffsetx = args.drawOffsetx or 0
        args.drawOffsety = args.drawOffsety or 0

        local savedColor = {love.graphics.getColor()}
        love.graphics.setColor(self.r, self.g, self.b, self.alpha)

        local scalex = (self.width / level.width) * (love.graphics.getWidth() / self.drawable:getWidth())
        local scaley = (self.height / level.height) * (love.graphics.getHeight() / self.drawable:getHeight())
        local x = self.xPercent * love.graphics.getWidth() + args.drawOffsetx
        local y = self.yPercent * love.graphics.getHeight() + args.drawOffsety
        love.graphics.draw(self.drawable, x, y, args.orientation, scalex, scaley, args.originx, args.originy)

        love.graphics.setColor(unpack(savedColor))
    end


    function imageObject:mouseIn(args)
        local sizex = (self.width / level.width) * love.graphics.getWidth()
        local sizey = (self.height / level.height) * love.graphics.getHeight()
        local x = self.xPercent * love.graphics.getWidth()
        local y = self.yPercent * love.graphics.getHeight()
        return mouseInRect({args[1], args[2]}, {x, y}, {sizex, sizey})
    end

    return imageObject
end

function buildRectObject(rect)
    local rectObject = {}
    rectObject.xPercent = rect.xarg.x / level.width
    rectObject.yPercent = rect.xarg.y / level.height
    rectObject.width = rect.xarg.width
    rectObject.height = rect.xarg.height
    rectObject.r = 255
    rectObject.g = 255
    rectObject.b = 255
    rectObject.alpha = 255

    function rectObject:draw(args)
        args = args or {}
        args.scalex = args.scalex or 1
        args.scaley = args.scaley or 1
        args.drawOffsetx = args.drawOffsetx or 0
        args.drawOffsety = args.drawOffsety or 0

        local savedColor = {love.graphics.getColor()}
        if (self.r and self.g and self.b) then
            love.graphics.setColor(self.r, self.g, self.b, self.alpha)
        end

        local width = (self.width / level.width) * love.graphics.getWidth() * args.scalex
        local height = (self.height / level.height) * love.graphics.getHeight() * args.scaley
        local x = self.xPercent * love.graphics.getWidth() + args.drawOffsetx
        local y = self.yPercent * love.graphics.getHeight() + args.drawOffsety
        love.graphics.rectangle("fill", x, y, width, height)

        love.graphics.setColor(unpack(savedColor))
    end

    function rectObject:mouseIn(args)
        local sizex = (self.width / level.width) * love.graphics.getWidth()
        local sizey = (self.height / level.height) * love.graphics.getHeight()
        return mouseInRect({args[1], args[2]}, {self.xPercent * love.graphics.getWidth(), self.yPercent * love.graphics.getHeight()}, {sizex, sizey})
    end

    return rectObject
end

function buildGroupObjects(group)
    local objects = {}
    for k, v in pairs(group) do
        if(v.xarg) then
            local object = nil

            if (v.label == "image") then
                object = buildImageObject(v)

            elseif (v.label == "rect") then
                object = buildRectObject(v)

            elseif (v.label == "path") then
                object = buildPathObject(v)
            end

            if object then
                object.id = v.xarg.id

                if v.xarg.style then
                    buildStyle(object, v.xarg.style)
                end


                if v.xarg.onclick then 
                    object.onclickStr = v.xarg.onclick:decodeEntities()
                end
                if v.xarg.onload then
                    object.onloadStr = v.xarg.onload:decodeEntities()
                end
                if v.xarg.update then
                    object.updateStr = v.xarg.update:decodeEntities()
                end

                function object:onClick(args)
                    args.mousex = args.mousex or 0
                    args.mousey = args.mousey or 0

                    if self.mouseIn and self:mouseIn({args.mousex, args.mousey}) then
                        if self.onclickStr then
                            local result = loadstr(self.onclickStr)(self)
                            if result == nil then
                                result = true
                            end
                            return result
                        end
                        return true
                    end
                end

                function object:onLoad()
                    if self.onload then
                        return loadstr(self.onloadStr)(self)
                    end
                end

                function object:update()
                    if self.updateStr then
                        return loadstr(self.updateStr)(self)
                    end
                end

                table.insert(objects, object)
            elseif (v.label == "g") then
                local nestedGroupObjects = buildGroupObjects(v) 
                for _,v in ipairs(nestedGroupObjects) do
                    table.insert(objects, v)
                end
            end
        end
    end
    return objects
end

function loadLevel(levelname)
    local xml = collect(love.filesystem.read(levelname))

    level = {}
    level.objects = {}

    for k,v in pairs(xml) do
        if v.label == "svg" then
            level.width = v.xarg.width:gsub("%a", "")
            level.height = v.xarg.height:gsub("%a", "")

            for k,v in pairs(v) do
                if(v.label == "sodipodi:namedview") then
                    level.bgcolor = {hexToRgb(v.xarg.pagecolor)}
                elseif v.label == "g" then
                    local loadedObjects = buildGroupObjects(v);
                    for k,v in pairs(loadedObjects) do
                        table.insert(level.objects, v)
                    end
                end
            end
        end
    end

    love.graphics.setBackgroundColor(level.bgcolor)
end