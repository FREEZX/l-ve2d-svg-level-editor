function loadstr(str, name)
    local f, err = loadstring("return function (self) " .. str .. " end", name or str)
    if f then
        return f()
    else
        return f, err
    end
end

function string:split(sep)
  local sep, fields = sep or ":", {}
  local pattern = string.format("([^%s]+)", sep)
  self:gsub(pattern, function(c) fields[#fields+1] = c end)
  return fields
end

function string:decodeEntities()
    local result = self:gsub("&quot;", '"')
    return result
end