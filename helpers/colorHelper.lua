function hexToRgb(hex)
    hex = hex:gsub("#", "")
    if hex:len() ~=6 then
        print("Hex invalid!")
        return nil
    end
    hex = hex:upper()
    local rComponent = hex:sub(1,2)
    local gComponent = hex:sub(3,4)
    local bComponent = hex:sub(5,6)
    local red = tonumber(rComponent, 16)
    local green = tonumber(gComponent, 16)
    local blue = tonumber(bComponent, 16)
    return red, green, blue
end