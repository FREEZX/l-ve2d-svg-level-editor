require('framework')

loadLevel('test.svg')

for k,v in pairs(level.objects) do
  if(v.onload) then
    v:onLoad()
  end
end

function love.update()
  for k,v in pairs(level.objects) do
    v:update()
  end
end

function love.draw()
  for k,v in pairs(level.objects) do
    v:draw()
  end
end

function love.mousepressed(x, y, button)
  for i=#level.objects, 1, -1 do
    local v = level.objects[i]
      --Disable propagation
    local res = v:onClick{mousex=x, mousey=y}
    if (res) then
      return
    end
  end
end